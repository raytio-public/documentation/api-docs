Raytio API documentation in OpenAPI 3.0 format

To install redoc-cli
```
npm install redoc-cli
```

To generate the API docs in static form
```
redoc-cli bundle ./s3/oas3.yaml -t ./s3/template_raytio.hbs -o ./s3/index.html --title 'Raytio API documentation'
```

Or using npx
```
npx redoc-cli bundle ./s3/oas3.yaml -t ./s3/template_raytio.hbs -o ./s3/index.html --title 'Raytio API documentation'
```

To upload to S3 (dev):
```
aws s3 sync ./s3 s3://raytio-dev-docs
```

To upload to S3 (prod):
```
aws s3 sync ./s3 s3://raytio-docs
```